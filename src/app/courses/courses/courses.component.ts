import { Course } from './../model/course';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {

  courses: Course[] = [
    {_id:'1', name:'Angular', category:'Front-end'},
    {_id:'2', name:'Spring Boot', category:'Back-end'},
    {_id:'3', name:'Git', category:'Full Stack'},
    {_id:'4', name:'Docker', category:'Dev Ops'},
  ];
  displayedColumns = ['name','category'];

  constructor() {
    //this.courses = []; /* Inicializado acima */
  }

    ngOnInit(): void {
  }

}
